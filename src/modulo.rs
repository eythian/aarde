pub trait ModuloSignedExt {
    fn modulo(&self, n: Self) -> usize;
}
macro_rules! modulo_signed_ext_impl {
    ($($t:ty)*) => ($(
            impl ModuloSignedExt for $t {
                #[inline]
                fn modulo(&self, n: Self) -> usize {
                    ((self % n + n) % n) as usize
                }
            }
            )*)
}
modulo_signed_ext_impl! { i8 i16 i32 i64 isize usize }
