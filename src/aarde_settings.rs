#[derive(Clone, Debug, Default)]
pub struct AardeSettings {
    pub soup_size: i64,
    pub stack_size: usize,
    pub slice_size: usize,
    pub min_tmpl_size: usize,
    pub tmpl_search_dist: usize,
}
