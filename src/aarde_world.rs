use aarde_settings::*;
/// This defines functions that are used by things that sit outside the
/// simulation looking in, or operating it at a very high level, e.g.
/// user interfaces.
pub trait AardeWorld {
    fn step(&mut self);
    fn get_iteration(&mut self) -> i64;
}

/// Connected to the `AardeWorld` trait, this has functions need for
/// other functions that can support the mechanics of running things, providing
/// world-specific actions.
pub trait ExecutableWorld {
    /// This allocates a daughter in the soup, putting the created one into
    /// the parent. It returns either Ok or Err depending on if the allocation
    /// succeeded.
    fn allocate_daughter_space(&mut self, size: i64, org: &mut Organism) -> Result<(), ()>;

    //fn get_current_organism(&self) -> Organism;

    fn curr_instr(&self) -> InstSet;

    fn curr_cpu_mut(&mut self) -> &mut OrganismCPU;
    fn curr_cpu(&self) -> &OrganismCPU;
    fn copy_soup_val(&mut self, src: i64, dst: i64);
    fn settings(&self) -> AardeSettings;

    //    fn get_soup(self) -> &[InstSet];
}

#[derive(Default, Debug)]
pub struct OrganismCPU {
    pub ip: i64,           // the instruction pointer, absolute in the soup
    pub stack: Box<[i64]>, // the stack
    pub stack_ptr: usize,  // the stack pointer
    pub ax: i64,
    pub bx: i64,
    pub cx: i64,
    pub dx: i64,
    pub err_count: usize,
}

pub struct Organism {
    pub start_point: i64,                // the address of the first instruction
    pub end_point: i64,                  // the address *after* the last instruction
    pub cpu: OrganismCPU,                // the CPU struct for the organism
    pub daughter: Option<Box<Organism>>, // the daughter organism
}

#[derive(Clone, PartialEq, Debug)]
#[allow(dead_code)]
pub enum InstSet {
    Empty, // uses for initialisation, basically a Nop with no templating
    Nop0,
    Nop1,
    PushA,
    PushB,
    PushC,
    PushD,
    PopA,
    PopB,
    PopC,
    PopD,
    MovDC,
    MovBA,
    Movii,
    SubCAB,
    SubAAC,
    IncA,
    IncB,
    IncC,
    DecC,
    Zero,
    Not0,
    Shl,
    Ifz,
    Jmpo,
    Jmpb,
    Call,
    Ret,
    Adro,
    Adrb,
    Adrf,
    Mal,
    Divide,
}
