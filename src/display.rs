use easycurses::Color::*;
use easycurses::*;

use std::time::Instant;

pub struct Display<'a> {
    screen: &'a mut EasyCurses,
    start_time: Instant,
    iteration: i64,
}

impl<'a> Display<'a> {
    pub fn init_display(screen: &mut EasyCurses) -> Display {
        screen.set_cursor_visibility(CursorVisibility::Invisible);
        screen.set_echo(false);
        screen.set_input_timeout(TimeoutMode::Immediate);
        Display {
            screen: screen,
            start_time: Instant::now(),
            iteration: 0,
        }
    }

    pub fn draw_top_bar(&mut self) {
        let (_, col_count) = self.screen.get_row_col_count();
        self.screen.move_rc(0, 0);
        self.screen.set_color_pair(colorpair!(Black on White));

        // TODO make the version num come from Cargo.toml
        let mut left_side = String::from("Aard v.0.1.0");
        let mut right_side = String::from(self.iteration.to_string());

        let mut out_string = String::new();

        // -2 for margins
        let filler_len =
            col_count as usize - (left_side.chars().count() + right_side.chars().count()) - 2;
        out_string.push_str(" ");
        out_string.push_str(&left_side);
        out_string.push_str(&" ".repeat(filler_len));
        out_string.push_str(&right_side);
        out_string.push_str(" ");
        self.screen.print(out_string);
    }

    pub fn paint(&mut self) {
        self.screen.refresh();
    }

    pub fn pause_for_input(&mut self) {
        self.screen.get_input();
    }

    pub fn get_input(&mut self) -> Option<Input> {
        self.screen.get_input()
    }

    pub fn set_iteration(&mut self, iteration: i64) {
        self.iteration = iteration;
    }
}
