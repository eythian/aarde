use aarde_settings::AardeSettings;
use aarde_world::InstSet;
use aarde_world::Organism;
use aarde_world::OrganismCPU;
use aarde_world::*;

use modulo::ModuloSignedExt;

pub struct InstTierra0 {
    iteration: i64,            // how many times has step() been called
    soup: Box<[InstSet]>,      // the soup
    exec_queue: Vec<Organism>, // all the organisms that can be executed
    current_org: usize,        // the one that is up next
    settings: AardeSettings,   // parameters for the simulation
    allocated_space: usize,    // space used in the soup
}

impl InstTierra0 {
    /// Initialise the Tierra original instruction set.
    ///
    /// This sets up the soup, loads the ancestor organism, and prepares
    /// everything to be run with the `step()` function.
    pub fn new(settings: AardeSettings) -> InstTierra0 {
        let mut init_soup = vec![InstSet::Empty; settings.soup_size as usize];
        let first_org = make_first_org();
        // The first organism goes into location 0 in the soup.
        let end_point = first_org.len();
        for i in 0..end_point {
            init_soup[i] = first_org[i].clone();
        }
        InstTierra0 {
            iteration: 0,
            soup: init_soup.into_boxed_slice(),
            settings: settings.clone(),
            current_org: 0,
            allocated_space: first_org.len(),
            exec_queue: vec![Organism {
                start_point: 0,
                end_point: end_point as i64,
                daughter: Option::None,
                cpu: OrganismCPU {
                    stack: vec![0; settings.stack_size].into_boxed_slice(),
                    ..Default::default()
                },
            }],
        }
    }
}

impl AardeWorld for InstTierra0 {
    /// Perform the actions that correspond to a single step of the world.
    fn step(&mut self) {
        //let mut exec_queue = &mut self.exec_queue;
        //let mut org = exec_queue.get_mut(self.current_org).unwrap();
        execute(self as &mut ExecutableWorld);

        //        self.current_org += 1;
        //        self.current_org %= exec_queue.len();
        self.iteration += 1;
    }

    /// Get the count of iterations. One iteration is one instruction
    /// executed (as opposed to a generation.)
    fn get_iteration(&mut self) -> i64 {
        self.iteration
    }
}

impl ExecutableWorld for InstTierra0 {
    fn allocate_daughter_space(&mut self, size: i64, org: &mut Organism) -> Result<(), ()> {
        Err(())
    }

    //fn get_curr_org(&mut self) -> Organism {
    //    self.exec_queue[self.current_org]
    //}

    fn curr_cpu_mut(&mut self) -> &mut OrganismCPU {
        &mut self.exec_queue[self.current_org].cpu
    }

    fn curr_cpu(&self) -> &OrganismCPU {
        &self.exec_queue[self.current_org].cpu
    }

    //    fn get_soup(&mut self) -> &[InstSet] {
    //        &*self.soup
    //    }

    fn curr_instr(&self) -> InstSet {
        self.soup[self.exec_queue[self.current_org]
                      .cpu
                      .ip
                      .modulo(self.settings.soup_size)].clone()
    }

    fn copy_soup_val(&mut self, src: i64, dst: i64) {
        // TODO need to do a pile of bookkeeping in here
        // and read-only verification etc.
        let usrc = src.modulo(self.settings.soup_size);
        let udst = dst.modulo(self.settings.soup_size);
        self.soup[udst] = self.soup[usrc].clone();
    }

    fn settings(&self) -> AardeSettings {
        self.settings.clone()
    }
}

impl InstTierra0 {
    fn soup(&mut self) -> &mut [InstSet] {
        &mut *self.soup
    }
}

impl Organism {
    fn cpu(&mut self) -> &mut OrganismCPU {
        &mut self.cpu
    }
}

impl OrganismCPU {
    fn stack_push(&mut self, val: i64, settings: &AardeSettings) {
        self.stack[self.stack_ptr] = val;
        self.stack_ptr = (self.stack_ptr + 1).modulo(settings.stack_size);
    }

    fn stack_pop(&mut self, settings: &AardeSettings) -> i64 {
        self.stack_ptr = (self.stack_ptr as i64 - 1).modulo(settings.stack_size as i64);
        self.stack[self.stack_ptr]
    }
}

/// Execute an instruction in the context of an organism's CPU
fn execute(world: &mut ExecutableWorld) {
    let mut raised_error = false;
    let settings = world.settings();
    match world.curr_instr() {
        InstSet::Nop0 | InstSet::Nop1 | InstSet::Empty => world.curr_cpu_mut().ip += 1, // we wrap around the ip afterwards if needed
        InstSet::PushA => {
            let r = world.curr_cpu().ax;
            world.curr_cpu_mut().stack_push(r, &settings);
            world.curr_cpu_mut().ip += 1;
        }
        InstSet::PushB => {
            let r = world.curr_cpu().bx;
            world.curr_cpu_mut().stack_push(r, &settings);
            world.curr_cpu_mut().ip += 1;
        }
        InstSet::PushC => {
            let r = world.curr_cpu().cx;
            world.curr_cpu_mut().stack_push(r, &settings);
            world.curr_cpu_mut().ip += 1;
        }
        InstSet::PushD => {
            let r = world.curr_cpu().dx;
            world.curr_cpu_mut().stack_push(r, &settings);
            world.curr_cpu_mut().ip += 1;
        }
        InstSet::PopA => {
            let r = world.curr_cpu_mut().stack_pop(&settings);
            world.curr_cpu_mut().ax = r;
            world.curr_cpu_mut().ip += 1;
        }
        InstSet::PopB => {
            let r = world.curr_cpu_mut().stack_pop(&settings);
            world.curr_cpu_mut().bx = r;
            world.curr_cpu_mut().ip += 1;
        }
        InstSet::PopC => {
            let r = world.curr_cpu_mut().stack_pop(&settings);
            world.curr_cpu_mut().cx = r;
            world.curr_cpu_mut().ip += 1;
        }
        InstSet::PopD => {
            let r = world.curr_cpu_mut().stack_pop(&settings);
            world.curr_cpu_mut().dx = r;
            world.curr_cpu_mut().ip += 1;
        }
        InstSet::MovDC => {
            let mut cpu = world.curr_cpu_mut();
            cpu.dx = cpu.cx;
            cpu.ip += 1;
        }
        InstSet::MovBA => {
            let mut cpu = world.curr_cpu_mut();
            cpu.bx = cpu.ax;
            cpu.ip += 1;
        }
        InstSet::Movii => {
            let src = world.curr_cpu().bx;
            let dst = world.curr_cpu().ax;
            world.copy_soup_val(src, dst); // TODO check for write permissions
            world.curr_cpu_mut().ip += 1;
        }
        InstSet::SubCAB => {
            let mut cpu = world.curr_cpu_mut();
            cpu.cx = cpu.ax - cpu.bx;
            cpu.ip += 1;
        }
        InstSet::SubAAC => {
            let mut cpu = world.curr_cpu_mut();
            cpu.ax = cpu.ax - cpu.cx;
            cpu.ip += 1;
        }
        InstSet::IncA => {
            let mut cpu = world.curr_cpu_mut();
            cpu.ax += 1;
            cpu.ip += 1;
        }
        InstSet::IncB => {
            let mut cpu = world.curr_cpu_mut();
            cpu.bx += 1;
            cpu.ip += 1;
        }
        InstSet::IncC => {
            let mut cpu = world.curr_cpu_mut();
            cpu.cx += 1;
            cpu.ip += 1;
        }
        InstSet::DecC => {
            let mut cpu = world.curr_cpu_mut();
            cpu.cx -= 1;
            cpu.ip += 1;
        }
        InstSet::Zero => {
            let mut cpu = world.curr_cpu_mut();
            cpu.cx = 0;
            cpu.ip += 1;
        }
        InstSet::Not0 => {
            let mut cpu = world.curr_cpu_mut();
            cpu.cx ^= 1; // flip low-order bit
            cpu.ip += 1;
        }
        InstSet::Shl => {
            let mut cpu = world.curr_cpu_mut();
            cpu.cx <<= 1;
            cpu.ip += 1;
        }
        InstSet::Ifz => {
            let mut cpu = world.curr_cpu_mut();
            cpu.ip = if cpu.cx == 0 { cpu.ip + 1 } else { cpu.ip + 2 };
        }
        //InstSet::Jmpo => {
        //    let mut cpu = world.curr_cpu_mut();
        //    match find_template(SearchDir::Outward, &settings, &world) {
        //        Err(err) => {
        //            raised_error = true;
        //            cpu.ip = err.next_ip;
        //        }
        //        Ok(res) => {
        //            cpu.ip = res.following_addr;
        //        }
        //    }
        //}
        //InstSet::Jmpb => match find_template(SearchDir::Backward, settings, &cpu, world.get_soup())
        //{
        //    Err(err) => {
        //        raised_error = true;
        //        cpu.ip = err.next_ip;
        //    }
        //    Ok(res) => {
        //        cpu.ip = res.following_addr;
        //    }
        //},
        //InstSet::Call => {
        //    match find_template(SearchDir::Outward, settings, &cpu, world.get_soup()) {
        //        Err(err) => {
        //            raised_error = true;
        //            cpu.ip = err.next_ip;
        //        }
        //        Ok(res) => {
        //            cpu.stack[stack_ptr as usize] = cpu.ip + res.template_size as i64 + 1;
        //            stack_ptr += 1;
        //            cpu.ip = res.following_addr;
        //        }
        //    }
        //}
        //InstSet::Ret => {
        //    if stack_ptr == 0 {
        //        // This is a difference from the original Tierra, aimed at
        //        // discouraging a bias to jumping to 0
        //        raised_error = true;
        //    }
        //    stack_ptr -= 1;
        //    cpu.ip = cpu.stack[stack_ptr.modulo(settings.stack_size as i64)];
        //}
        //InstSet::Adro => {
        //    match find_template(SearchDir::Outward, settings, &cpu, world.get_soup()) {
        //        Err(err) => {
        //            raised_error = true;
        //            cpu.ip = err.next_ip;
        //        }
        //        Ok(res) => {
        //            cpu.ax = res.following_addr;
        //            cpu.cx = res.template_size as i64;
        //            cpu.ip = res.next_ip;
        //        }
        //    }
        //}
        //InstSet::Adrb => match find_template(SearchDir::Backward, settings, &cpu, world.get_soup())
        //{
        //    Err(err) => {
        //        raised_error = true;
        //        cpu.ip = err.next_ip;
        //    }
        //    Ok(res) => {
        //        cpu.ax = res.following_addr;
        //        cpu.cx = res.template_size as i64;
        //        cpu.ip = res.next_ip;
        //    }
        //},
        //InstSet::Adrf => {
        //    match find_template(SearchDir::Forward, settings, &cpu, world.get_soup()) {
        //        Err(err) => {
        //            raised_error = true;
        //            cpu.ip = err.next_ip;
        //        }
        //        Ok(res) => {
        //            cpu.ax = res.following_addr;
        //            cpu.cx = res.template_size as i64;
        //            cpu.ip = res.next_ip;
        //        }
        //    }
        //}
        _ => (), // TODO remove when all insts are defined
    }
    //cpu.ip = cpu.ip.modulo(settings.soup_size) as i64;
    //cpu.stack_ptr = stack_ptr.modulo(settings.stack_size as i64);
    //if raised_error {
    //    cpu.err_count += 1;
    //}
}

fn make_first_org() -> Vec<InstSet> {
    // for now this is hard coded. Eventually it should parse a file,
    // once I've written a "compiler" for that.
    let first_org = vec![
        InstSet::Nop1,
        InstSet::Nop1,
        InstSet::Nop1,
        InstSet::Nop1,
        InstSet::Zero,
        InstSet::Not0,
        InstSet::Shl,
        InstSet::Shl,
        InstSet::MovDC,
        InstSet::Adrb,
        InstSet::Nop0,
        InstSet::Nop0,
        InstSet::Nop0,
        InstSet::Nop0,
        InstSet::SubAAC,
        InstSet::MovBA,
        InstSet::Adrf,
        InstSet::Nop0,
        InstSet::Nop0,
        InstSet::Nop0,
        InstSet::Nop1,
        InstSet::IncA,
        InstSet::SubCAB,
        InstSet::Nop1,
        InstSet::Nop1,
        InstSet::Nop0,
        InstSet::Nop1,
        InstSet::Mal,
        InstSet::Call,
        InstSet::Nop0,
        InstSet::Nop0,
        InstSet::Nop1,
        InstSet::Nop1,
        InstSet::Divide,
        InstSet::Jmpo,
        InstSet::Nop0,
        InstSet::Nop0,
        InstSet::Nop1,
        InstSet::Nop0,
        InstSet::Ifz,
        InstSet::Nop1,
        InstSet::Nop1,
        InstSet::Nop0,
        InstSet::Nop0,
        InstSet::PushA,
        InstSet::PushB,
        InstSet::PushC,
        InstSet::Nop1,
        InstSet::Nop0,
        InstSet::Nop1,
        InstSet::Nop0,
        InstSet::Movii,
        InstSet::DecC,
        InstSet::Ifz,
        InstSet::Jmpo,
        InstSet::Nop0,
        InstSet::Nop1,
        InstSet::Nop0,
        InstSet::Nop0,
        InstSet::IncA,
        InstSet::IncB,
        InstSet::Jmpo,
        InstSet::Nop0,
        InstSet::Nop1,
        InstSet::Nop0,
        InstSet::Nop1,
        InstSet::Ifz,
        InstSet::Nop1,
        InstSet::Nop0,
        InstSet::Nop1,
        InstSet::Nop1,
        InstSet::PopC,
        InstSet::PopB,
        InstSet::PopA,
        InstSet::Ret,
        InstSet::Nop1,
        InstSet::Nop1,
        InstSet::Nop1,
        InstSet::Nop0,
        InstSet::Ifz,
    ];
    return first_org;
}

#[derive(PartialEq, Debug, Clone)]
enum SearchDir {
    Forward,
    Backward,
    Outward,
}

struct SearchResult {
    /// The address one higher than the found template (no matter the direction)
    following_addr: i64,
    /// The address after the template that was used for searching
    next_ip: i64,
    /// The size of the template used
    template_size: usize,
    /// The direction the complementary template was found in, one of
    /// `SearchDir::Forward` or `SearchDir::Backward`
    direction: SearchDir,
}

struct FailedSearchResult {
    /// The address to resume execution from
    next_ip: i64,
}

/// Search through the soup in the specified direction looking for a template
/// that matches the compliment of the one that starts right after `ip`.
///
/// If it's found, it returns a tuple. The first value is the address
/// immediately after the template (even if searching backwards, it's
/// the address after the highest addressed part of the template, not on the
/// other side of it.) The second value is the size of the template.
///
/// Will return `None` if either there's no initial template, or if none
/// is found in the search size.
fn find_template(
    dir: SearchDir,
    settings: &AardeSettings,
    cpu: &OrganismCPU,
    soup: &[InstSet],
) -> Result<SearchResult, FailedSearchResult> {
    // first, find our initial template
    let mut tmpl_fwd = Vec::new();
    let mut init_tmpl_idx = cpu.ip;
    loop {
        init_tmpl_idx += 1;
        tmpl_fwd.push(
            match soup[init_tmpl_idx.modulo(settings.soup_size) as usize] {
                InstSet::Nop1 => InstSet::Nop0, // inverted
                InstSet::Nop0 => InstSet::Nop1,
                _ => break,
            },
        );
    }
    let tmpl_size = tmpl_fwd.len();
    if tmpl_size < settings.min_tmpl_size {
        return Err(FailedSearchResult {
            next_ip: init_tmpl_idx,
        });
    }
    let mut tmpl_rev = tmpl_fwd.clone();
    tmpl_rev.reverse();

    // Now search
    let mut searches = 0;
    let max_searches = match dir {
        SearchDir::Outward => settings.tmpl_search_dist * 2,
        _ => settings.tmpl_search_dist,
    };

    #[derive(Clone, Debug)]
    struct SearchStatus {
        tmpl: Vec<InstSet>,
        loc: i64,             // location in the soup we want to look at next
        match_point: usize,   // location in template we're comparing next
        direction: SearchDir, // bookkeeping so we know where it was found
        increment: i64,       // which way to move in the soup
    }

    let mut status_fwd = SearchStatus {
        tmpl: tmpl_fwd,
        loc: cpu.ip + tmpl_size as i64 + 1,
        match_point: 0,
        direction: SearchDir::Forward,
        increment: 1,
    };
    let mut status_back = SearchStatus {
        tmpl: tmpl_rev,
        loc: cpu.ip - 1,
        match_point: 0,
        direction: SearchDir::Backward,
        increment: -1,
    };
    // define our initial direction
    let mut curr_dir = match dir {
        SearchDir::Outward => SearchDir::Forward,
        _ => dir.clone(),
    };
    let final_status = loop {
        let ref mut status = match curr_dir {
            SearchDir::Forward => &mut status_fwd,
            SearchDir::Backward => &mut status_back,
            _ => panic!("Impossible state in searching"),
        };

        // does the inst at loc match our template at match point?
        if soup[status.loc.modulo(settings.soup_size)] == status.tmpl[status.match_point] {
            if status.match_point == tmpl_size - 1 {
                break status.clone();
            }
            status.match_point += 1;
        } else {
            status.match_point = 0;
        }
        status.loc += status.increment;
        searches += 1;
        if searches > max_searches {
            return Err(FailedSearchResult {
                next_ip: init_tmpl_idx,
            });
        }

        if dir == SearchDir::Outward {
            curr_dir = match curr_dir {
                SearchDir::Forward => SearchDir::Backward,
                SearchDir::Backward => SearchDir::Forward,
                _ => panic!("Impossible state in searching"),
            }
        }
    };

    Ok(SearchResult {
        following_addr: match final_status.direction {
            SearchDir::Forward => final_status.loc + 1,
            SearchDir::Backward => final_status.loc + tmpl_size as i64,
            _ => panic!("Impossible state in search result"),
        },
        next_ip: init_tmpl_idx,
        template_size: tmpl_size,
        direction: final_status.direction.clone(),
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_default_cpu(settings: &AardeSettings) -> OrganismCPU {
        OrganismCPU {
            stack: vec![0; settings.stack_size].into_boxed_slice(),
            ip: 49,
            ..Default::default()
        }
    }

    fn get_settings() -> AardeSettings {
        AardeSettings {
            soup_size: 100,
            stack_size: 10,
            slice_size: 1,
            min_tmpl_size: 1,
            tmpl_search_dist: 50,
        }
    }

    fn get_template_soup(settings: &AardeSettings) -> Box<[InstSet]> {
        let mut init_soup = vec![InstSet::Empty; settings.soup_size as usize];
        // we put a template from 50-52, and its compliment at
        // 25 and at 80.
        init_soup[50] = InstSet::Nop0;
        init_soup[51] = InstSet::Nop1;

        init_soup[25] = InstSet::Nop1;
        init_soup[26] = InstSet::Nop0;
        init_soup[80] = InstSet::Nop1;
        init_soup[81] = InstSet::Nop0;

        // we put another template at 60 and its compliment wraps over
        // the edge of the soup
        init_soup[60] = InstSet::Nop0;
        init_soup[61] = InstSet::Nop0;

        init_soup[99] = InstSet::Nop1;
        init_soup[0] = InstSet::Nop1;
        init_soup.into_boxed_slice()
    }

    #[test]
    fn find_template_forward() {
        let settings = get_settings();
        let cpu = get_default_cpu(&settings);
        let soup = get_template_soup(&settings);

        let result = find_template(SearchDir::Forward, &settings, &cpu, &soup);
        match result {
            Err(_) => panic!("Expected some but got None"),
            Ok(result) => {
                assert_eq!(result.following_addr, 82);
                assert_eq!(result.template_size, 2);
                assert_eq!(result.direction, SearchDir::Forward);
                assert_eq!(result.next_ip, 52);
            }
        }
    }

    #[test]
    fn find_template_backward() {
        let settings = get_settings();
        let cpu = get_default_cpu(&settings);
        let soup = get_template_soup(&settings);

        let result = find_template(SearchDir::Backward, &settings, &cpu, &soup);
        match result {
            Err(_) => panic!("Expected some but got None"),
            Ok(result) => {
                assert_eq!(result.following_addr, 27);
                assert_eq!(result.template_size, 2);
                assert_eq!(result.direction, SearchDir::Backward);
                assert_eq!(result.next_ip, 52);
            }
        }
    }

    #[test]
    fn find_template_outward() {
        let settings = get_settings();
        let cpu = get_default_cpu(&settings);
        let soup = get_template_soup(&settings);

        let result = find_template(SearchDir::Outward, &settings, &cpu, &soup);
        match result {
            Err(_) => panic!("Expected some but got None"),
            Ok(result) => {
                assert_eq!(result.following_addr, 27);
                assert_eq!(result.template_size, 2);
                assert_eq!(result.direction, SearchDir::Backward);
                assert_eq!(result.next_ip, 52);
            }
        }
    }

    #[test]
    fn find_template_wrapping() {
        let settings = get_settings();
        let mut cpu = get_default_cpu(&settings);
        let soup = get_template_soup(&settings);
        cpu.ip = 59;

        let result = find_template(SearchDir::Forward, &settings, &cpu, &soup);
        match result {
            Err(_) => panic!("Expected some but got None"),
            Ok(result) => {
                // note that it returns 101, so that organism maths can
                // still work. All access will be wrapped into the soup.
                assert_eq!(result.following_addr, 101);
                assert_eq!(result.template_size, 2);
                assert_eq!(result.direction, SearchDir::Forward);
                assert_eq!(result.next_ip, 62);
            }
        }
    }

    #[test]
    fn find_template_nomatch() {
        let mut settings = get_settings();
        let cpu = get_default_cpu(&settings);
        let soup = get_template_soup(&settings);
        settings.tmpl_search_dist = 10;

        let result = find_template(SearchDir::Forward, &settings, &cpu, &soup);
        match result {
            Err(res) => assert_eq!(res.next_ip, 52, "Failed search, right IP"),
            Ok(_) => panic!("Got a result when I shouldn't have"),
        }
    }

    fn make_world(soup: Vec<InstSet>, cpu: OrganismCPU, settings: AardeSettings) -> InstTierra0 {
        InstTierra0 {
            iteration: 0,
            soup: soup.into_boxed_slice(),
            settings: settings.clone(),
            current_org: 0,
            allocated_space: 0,
            exec_queue: vec![Organism {
                start_point: 0,
                end_point: 0,
                daughter: Option::None,
                cpu: cpu,
            }],
        }
    }

    #[test]
    fn nop_instr() {
        let settings = get_settings();
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::Nop0;
        world.soup()[50] = InstSet::Nop1;

        execute(&mut world);
        assert_eq!(world.curr_cpu().ip, 50, "Nop0");
        execute(&mut world);
        assert_eq!(world.curr_cpu().ip, 51, "Nop1");
        execute(&mut world); // test Empty
        assert_eq!(world.curr_cpu().ip, 52, "Empty");
        assert_eq!(world.curr_cpu().ax, 0, "No AX change");
        assert_eq!(world.curr_cpu().bx, 0, "No BX change");
        assert_eq!(world.curr_cpu().cx, 0, "No CX change");
        assert_eq!(world.curr_cpu().dx, 0, "No DX change");
    }

    #[test]
    fn push_pop_instr() {
        let settings = get_settings();
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::IncA; //  AX == 1
        world.soup()[50] = InstSet::PushA; // [1]
        world.soup()[51] = InstSet::MovBA; // BX == 1
        world.soup()[52] = InstSet::IncB; //  BX == 2
        world.soup()[53] = InstSet::PushB; // [1,2]
        world.soup()[54] = InstSet::PopC; //  CX == 2, [1]
        world.soup()[55] = InstSet::IncC; //  CX == 3
        world.soup()[56] = InstSet::PushC; // [1,3]
        world.soup()[57] = InstSet::PopD; //  DX == 3, [1]
        world.soup()[58] = InstSet::DecC; //  CX == 2
        world.soup()[59] = InstSet::MovDC; // DX == 2
        world.soup()[60] = InstSet::PushD; // [1,2]
        world.soup()[61] = InstSet::PopA; //  AX == 2, [1]
        world.soup()[62] = InstSet::PopB; //  BX == 1, []

        execute(&mut world); // IncA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ax, 1, "IncA");
        execute(&mut world); // PushA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 1, "PushA - stack ptr");
        assert_eq!(world.curr_cpu().stack[0], 1, "PushA - stack entry");
        execute(&mut world); // MovBA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().bx, 1, "MovBA");
        execute(&mut world); // IncB
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().bx, 2, "IncB");
        execute(&mut world); // PushB
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 2, "PushB - stack ptr");
        assert_eq!(world.curr_cpu().stack[1], 2, "PushB - stack entry");
        execute(&mut world); // PopC
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 1, "PopC - stack ptr");
        assert_eq!(world.curr_cpu().cx, 2, "PopC - CX");
        execute(&mut world); // IncC
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().cx, 3, "IncC");
        execute(&mut world); // PushC
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(
            world.curr_cpu().stack_ptr,
            2,
            "PushC - stack ptr, world.curr_cpu() is {:?}",
            world.curr_cpu()
        );
        assert_eq!(world.curr_cpu().stack[1], 3, "PushC - stack entry");
        execute(&mut world); // PopD
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 1, "PopD - stack ptr");
        assert_eq!(world.curr_cpu().dx, 3, "PopD - DX");
        execute(&mut world); // DecC
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().cx, 2, "DecC");
        execute(&mut world); // MovDC
        execute(&mut world); // PushD
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 2, "PushD - stack ptr");
        assert_eq!(world.curr_cpu().stack[1], 2, "PushD - stack entry");
        execute(&mut world); // PopA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 1, "PopA - stack ptr");
        assert_eq!(world.curr_cpu().ax, 2, "PopA - AX");
        execute(&mut world); // PopB
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 0, "PopB - stack ptr");
        assert_eq!(world.curr_cpu().bx, 1, "PopB - BX");

        // test stack wrapping
        world.soup()[63] = InstSet::PopA;
        world.soup()[64] = InstSet::PopB;
        world.soup()[65] = InstSet::PopC;
        world.soup()[66] = InstSet::PopD;
        world.soup()[67] = InstSet::IncA;
        world.soup()[68] = InstSet::IncA; // AX == 2
        world.soup()[69] = InstSet::PushA;
        world.soup()[70] = InstSet::PushA;
        world.soup()[71] = InstSet::PushA;
        world.soup()[72] = InstSet::PushA;
        world.soup()[73] = InstSet::PushA;

        execute(&mut world); // PopA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 9, "PopA - stack ptr");
        assert_eq!(world.curr_cpu().ax, 0, "PopA - AX");
        execute(&mut world); // PopB
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 8, "PopB - stack ptr");
        assert_eq!(world.curr_cpu().bx, 0, "PopB - BX");
        execute(&mut world); // PopC
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 7, "PopC - stack ptr");
        assert_eq!(world.curr_cpu().cx, 0, "PopC - CX");
        execute(&mut world); // PopD
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 6, "PopD - stack ptr");
        assert_eq!(world.curr_cpu().dx, 0, "PopD - DX");

        execute(&mut world); // IncA
        execute(&mut world); // IncA
        execute(&mut world); // PushA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 7, "PushA - stack ptr");
        assert_eq!(world.curr_cpu().stack[6], 2, "PushA - stack entry");
        execute(&mut world); // PushA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 8, "PushA - stack ptr");
        assert_eq!(world.curr_cpu().stack[7], 2, "PushA - stack entry");
        execute(&mut world); // PushA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 9, "PushA - stack ptr");
        assert_eq!(world.curr_cpu().stack[8], 2, "PushA - stack entry");
        execute(&mut world); // PushA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 0, "PushA - stack ptr");
        assert_eq!(world.curr_cpu().stack[9], 2, "PushA - stack entry");
        execute(&mut world); // PushA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().stack_ptr, 1, "PushA - stack ptr");
        assert_eq!(world.curr_cpu().stack[0], 2, "PushA - stack entry");
    }

    #[test]
    fn mov_instr() {
        let settings = get_settings();
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::IncA;
        world.soup()[50] = InstSet::IncA;
        world.soup()[51] = InstSet::IncA;
        world.soup()[52] = InstSet::MovBA;
        world.soup()[53] = InstSet::IncC;
        world.soup()[54] = InstSet::IncC;
        world.soup()[55] = InstSet::MovDC;

        execute(&mut world); // IncA
        execute(&mut world); // IncA
        execute(&mut world); // IncA
        execute(&mut world); // MovBA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().bx, 3, "MovBA - BX");
        execute(&mut world); // IncC
        execute(&mut world); // IncC
        execute(&mut world); // MovDC
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().dx, 2, "MovDC - DX");

        world.soup()[56] = InstSet::IncA; // AX == 4
        world.soup()[57] = InstSet::Movii; // world.soup()[3] -> soup[4]
        world.soup()[3] = InstSet::Nop0;

        execute(&mut world); // IncA
        execute(&mut world); // Movii
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.soup()[4], InstSet::Nop0, "Movii - world.soup()[4]");
    }

    #[test]
    fn sub_instr() {
        let settings = get_settings();
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::IncA; //  AX == 1
        world.soup()[50] = InstSet::IncB;
        world.soup()[51] = InstSet::IncB; //  BX == 2
        world.soup()[52] = InstSet::SubCAB; // CX == -1

        execute(&mut world); // IncA
        execute(&mut world); // IncB
        execute(&mut world); // IncB
        execute(&mut world); // SubCAB
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 53, "SubCAB - ip");
        assert_eq!(world.curr_cpu().cx, -1, "SubCAB - cx");

        world.soup()[53] = InstSet::SubAAC; // AX == 1 - (-1) == 2
        execute(&mut world); // SubAAC
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 54, "SubAAC - ip");
        assert_eq!(world.curr_cpu().ax, 2, "SubAAC - ax");
    }

    #[test]
    fn inc_dec_instr() {
        let settings = get_settings();
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::IncA; // AX == 1
        world.soup()[50] = InstSet::IncB; // BX == 1
        world.soup()[51] = InstSet::IncB; // BX == 2
        world.soup()[52] = InstSet::IncC; // CX == 1
        world.soup()[53] = InstSet::IncC; // CX == 2
        world.soup()[54] = InstSet::IncC; // CX == 3
        world.soup()[55] = InstSet::DecC; // CX == 2

        execute(&mut world); // IncA
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 50, "IncA - ip");
        assert_eq!(world.curr_cpu().ax, 1, "IncA - ax");

        execute(&mut world); // IncB
        execute(&mut world); // IncB
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 52, "IncB - ip");
        assert_eq!(world.curr_cpu().bx, 2, "IncB - bx");

        execute(&mut world); // IncC
        execute(&mut world); // IncC
        execute(&mut world); // IncC
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 55, "IncC - ip");
        assert_eq!(world.curr_cpu().cx, 3, "IncC - cx");

        execute(&mut world); // DecC
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 56, "DecC - ip");
        assert_eq!(world.curr_cpu().cx, 2, "DecC - cx");
    }

    #[test]
    fn misc_numerical() {
        let settings = get_settings();
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::IncC; // CX == 1
        world.soup()[50] = InstSet::Shl; //  CX == 2
        world.soup()[51] = InstSet::Shl; //  CX == 4
        world.soup()[52] = InstSet::Not0; // CX == 5
        world.soup()[53] = InstSet::Zero; // CX == 0

        execute(&mut world); // IncC
        execute(&mut world); // Shl
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 51, "Shl - ip");
        assert_eq!(world.curr_cpu().cx, 2, "Shl - cx");

        execute(&mut world); // Shl
        execute(&mut world); // Not0
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 53, "Not0 - ip");
        assert_eq!(world.curr_cpu().cx, 5, "Not0 - cx");

        execute(&mut world); // Zero
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 54, "Zero - ip");
        assert_eq!(world.curr_cpu().cx, 0, "Zero - cx");
    }

    #[test]
    fn jumps() {
        // ifz, jmpo, jmpb, call, ret
        let mut settings = get_settings();
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        settings.tmpl_search_dist = 40; // avoid wrapping around
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::Ifz; // Won't jump
        world.soup()[50] = InstSet::IncC; // CX == 1
        world.soup()[51] = InstSet::Ifz; // Will jump
        world.soup()[52] = InstSet::Nop0;

        execute(&mut world); // Ifz
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 50, "Ifz - ip");
        execute(&mut world); // IncC
        execute(&mut world); // Ifz
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 53, "Ifz - ip");

        // jump outward
        world.soup()[53] = InstSet::Jmpo;
        world.soup()[54] = InstSet::Nop1;
        world.soup()[55] = InstSet::Nop1;

        world.soup()[30] = InstSet::Nop0;
        world.soup()[31] = InstSet::Nop0;
        world.soup()[32] = InstSet::Jmpo;
        world.soup()[33] = InstSet::Nop0;
        world.soup()[34] = InstSet::Nop1;

        world.soup()[40] = InstSet::Nop1;
        world.soup()[41] = InstSet::Nop0;

        execute(&mut world); // Jmpo
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 32, "Jmpo (backwards) - ip");

        execute(&mut world); // Jmpo
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 42, "Jmpo (forwards) - ip");

        // jump back
        world.soup()[42] = InstSet::Jmpb;
        world.soup()[43] = InstSet::Nop0;
        world.soup()[44] = InstSet::Nop1;
        world.soup()[45] = InstSet::Nop1;

        world.soup()[10] = InstSet::Nop1;
        world.soup()[11] = InstSet::Nop0;
        world.soup()[12] = InstSet::Nop0;

        execute(&mut world); // Jmpb
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 13, "Jmpb - ip");

        // call, ret
        println!(" --- Starting call");
        let mut settings = get_settings();
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        settings.tmpl_search_dist = 40; // avoid wrapping around
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::Call; // IP == 64
        world.soup()[50] = InstSet::Nop0;
        world.soup()[51] = InstSet::Nop1;
        world.soup()[52] = InstSet::Nop0;

        world.soup()[61] = InstSet::Nop1;
        world.soup()[62] = InstSet::Nop0;
        world.soup()[63] = InstSet::Nop1;
        world.soup()[64] = InstSet::Ret; //  IP == 53

        // call backwards
        world.soup()[53] = InstSet::Call; // IP == 33
        world.soup()[54] = InstSet::Nop0;
        world.soup()[55] = InstSet::Nop1;
        world.soup()[56] = InstSet::Nop1;

        world.soup()[30] = InstSet::Nop1;
        world.soup()[31] = InstSet::Nop0;
        world.soup()[32] = InstSet::Nop0;

        execute(&mut world); // Call
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 64, "Call - ip");
        assert_eq!(world.curr_cpu().stack_ptr, 1, "Call - stack ptr");
        assert_eq!(world.curr_cpu().stack[0], 53, "Call - stack ret value");

        execute(&mut world); // Ret
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 53, "Ret - ip");
        assert_eq!(world.curr_cpu().stack_ptr, 0, "Ret - stack ptr");

        execute(&mut world); // Call
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ip, 33, "Call - ip");
        assert_eq!(world.curr_cpu().stack_ptr, 1, "Call - stack ptr");
        assert_eq!(world.curr_cpu().stack[0], 57, "Call - stack ret value");

        // failure modes
        println!(" --- Starting failure tests");
        let mut settings = get_settings();
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        settings.tmpl_search_dist = 40; // avoid wrapping around
        settings.min_tmpl_size = 2;
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::Jmpo;
        world.soup()[50] = InstSet::Nop0;
        world.soup()[51] = InstSet::Jmpb;
        world.soup()[52] = InstSet::Nop1;
        world.soup()[53] = InstSet::Call;
        world.soup()[54] = InstSet::Nop0;

        execute(&mut world); // Jmpo
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().err_count, 1, "Jmpo failed - err count");
        assert_eq!(world.curr_cpu().ip, 51, "Jmpo failed - ip");
        execute(&mut world); // Jmpb
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().err_count, 2, "Jmpb failed - err count");
        assert_eq!(world.curr_cpu().ip, 53, "Jmpb - ip");
        execute(&mut world); // Call
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().err_count, 3, "Call failed - err count");
        assert_eq!(world.curr_cpu().ip, 55, "Call - ip");
        assert_eq!(world.curr_cpu().stack_ptr, 0, "Call - stack ptr");
    }

    #[test]
    fn searching() {
        // adro, adrb, adrf
        let mut settings = get_settings();
        settings.tmpl_search_dist = 40; // avoid wrapping around
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::Adro;
        world.soup()[50] = InstSet::Nop0;
        world.soup()[51] = InstSet::Nop0;
        world.soup()[52] = InstSet::Nop0;
        world.soup()[53] = InstSet::Adro;
        world.soup()[54] = InstSet::Nop0;
        world.soup()[55] = InstSet::Nop0;
        world.soup()[56] = InstSet::Nop1;
        world.soup()[57] = InstSet::Adrb;
        world.soup()[58] = InstSet::Nop0;
        world.soup()[59] = InstSet::Nop1;
        world.soup()[60] = InstSet::Nop0;
        world.soup()[61] = InstSet::Adrf;
        world.soup()[62] = InstSet::Nop0;
        world.soup()[63] = InstSet::Nop1;
        world.soup()[64] = InstSet::Nop1;

        world.soup()[30] = InstSet::Nop1; // Adro 1
        world.soup()[31] = InstSet::Nop1;
        world.soup()[32] = InstSet::Nop1;

        world.soup()[70] = InstSet::Nop1; // Adro 2
        world.soup()[71] = InstSet::Nop1;
        world.soup()[72] = InstSet::Nop0;

        world.soup()[34] = InstSet::Nop1; // Adrb
        world.soup()[35] = InstSet::Nop0;
        world.soup()[36] = InstSet::Nop1;

        world.soup()[75] = InstSet::Nop1; // Adrf
        world.soup()[76] = InstSet::Nop0;
        world.soup()[77] = InstSet::Nop0;

        execute(&mut world); // Adro 1
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ax, 33, "Adro (back) - AX");
        assert_eq!(world.curr_cpu().cx, 3, "Adro (back) - AX");

        execute(&mut world); // Adro 2
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ax, 73, "Adro (forward) - AX");
        assert_eq!(world.curr_cpu().cx, 3, "Adro (forward) - AX");

        execute(&mut world); // Adrb
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ax, 37, "Adrb - AX");
        assert_eq!(world.curr_cpu().cx, 3, "Adrb - AX");

        execute(&mut world); // Adrf
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().ax, 78, "Adrf - AX");
        assert_eq!(world.curr_cpu().cx, 3, "Adrf - AX");

        println!(" --- Testing failure modes");
        let mut settings = get_settings();
        settings.min_tmpl_size = 2;
        settings.tmpl_search_dist = 40; // avoid wrapping around
        let mut soup = vec![InstSet::Empty; settings.soup_size as usize];
        let mut cpu = get_default_cpu(&settings);
        let mut world = make_world(soup, cpu, settings);

        world.soup()[49] = InstSet::Adro;
        world.soup()[50] = InstSet::Nop0;
        world.soup()[51] = InstSet::Adrb;
        world.soup()[52] = InstSet::Nop1;
        world.soup()[53] = InstSet::Adrf;
        world.soup()[54] = InstSet::Nop0;

        execute(&mut world); // Adro
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().err_count, 1, "Adro failed - err count");
        assert_eq!(world.curr_cpu().ip, 51, "Adro failed - ip");
        execute(&mut world); // Adrb
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().err_count, 2, "Adrb failed - err count");
        assert_eq!(world.curr_cpu().ip, 53, "Adrb - ip");
        execute(&mut world); // Adrf
        println!("world.curr_cpu() is {:?}", world.curr_cpu());
        assert_eq!(world.curr_cpu().err_count, 3, "Adrf failed - err count");
        assert_eq!(world.curr_cpu().ip, 55, "Adrf - ip");
    }
}
