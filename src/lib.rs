#[macro_use]
extern crate easycurses;

use easycurses::*;
use std::time::Duration;
use std::time::Instant;

use aarde_settings::AardeSettings;
use aarde_world::*;
use display::*;
use inst_tierra_0::*;

pub fn run() {
    let settings = AardeSettings {
        soup_size: 1_000_000,
        stack_size: 10,
        slice_size: 5,
        min_tmpl_size: 1,
        tmpl_search_dist: 1_000,
    };

    let mut screen = EasyCurses::initialize_system().unwrap();

    let mut world = InstTierra0::new(settings);

    let mut disp = Display::init_display(&mut screen);

    event_loop(&mut disp, &mut world);
}

fn event_loop(disp: &mut Display, world: &mut AardeWorld) {
    let frame_rate = Duration::new(1, 0)
        .checked_div(5)
        .expect("This shouldn't fail");
    let mut last_paint = Instant::now();
    let mut paint_anyway = true;
    loop {
        world.step();

        let mut should_quit = false;
        while let Some(input) = disp.get_input() {
            match input {
                Input::Character('q') => {
                    paint_anyway = true;
                    should_quit = true;
                }
                _ => (),
            }
        }

        sync_world_display(disp, world);

        let elapsed = last_paint.elapsed();
        let should_paint = None == frame_rate.checked_sub(elapsed);
        if should_paint || paint_anyway {
            disp.draw_top_bar();
            disp.paint();

            last_paint = Instant::now();
            paint_anyway = false;
        }

        if should_quit {
            break;
        }
    }
}

fn sync_world_display(disp: &mut Display, world: &mut AardeWorld) {
    disp.set_iteration(world.get_iteration());
}

mod aarde_settings;
mod aarde_world;
mod display;
mod inst_tierra_0;
mod modulo;
