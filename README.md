[![pipeline status](https://gitlab.com/eythian/aarde/badges/master/pipeline.svg)](https://gitlab.com/eythian/aarde/commits/master)

# Aarde
An artificial life environment
## What is this?
This is a reimplementation of the [Tierra](http://life.ou.edu/tierra/)
artificial life system. It executes assembly-code-like organisms in a
virtual machine type environment where they can reproduce themselves and
interact with each other.
## Why?
This has already been done, so why do it again?

The reasons are twofold:

1. the implementation is in [Rust](https://www.rust-lang.org) and it's a
chance for me to learn the language on a project that's larger than a toy;
2. playing with artificial life systems is fun, they evolve in interesting
ways.

Once this is in a state where it can get results like those of the original
Tierra, then I can experiment with different instruction sets or ways of
executing them. For example, adding things to encourage multi-cellular
behaviour, communication, dominant/recessive genes, etc.
## What's the current status?
It's not yet complete, decoding of the instructions is nearly done but there's
no mutation, no informative user interface, no state saving or loading, or
most other things that'd make it useful. These will all be coming soon.
## Other things
### Instruction set
The initial instruction set being implemented is the Tierra [instruction set
0](http://life.ou.edu/pubs/doc/index.html#Details), which is quite limited
but also quite simple.
